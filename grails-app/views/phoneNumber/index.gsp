
<%@ page import="com.twc.PhoneNumber" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'phoneNumber.label', default: 'PhoneNumber')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-phoneNumber" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-phoneNumber" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="phoneNumber.customer.label" default="Customer" /></th>
					
						<g:sortableColumn property="isActive" title="${message(code: 'phoneNumber.isActive.label', default: 'Is Active')}" />
					
						<g:sortableColumn property="number" title="${message(code: 'phoneNumber.number.label', default: 'Number')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${phoneNumberInstanceList}" status="i" var="phoneNumberInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${phoneNumberInstance.id}">${fieldValue(bean: phoneNumberInstance, field: "customer")}</g:link></td>
					
						<td><g:formatBoolean boolean="${phoneNumberInstance.isActive}" /></td>
					
						<td>${fieldValue(bean: phoneNumberInstance, field: "number")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${phoneNumberInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
