<%@ page import="com.twc.PhoneNumber" %>



<div class="fieldcontain ${hasErrors(bean: phoneNumberInstance, field: 'customer', 'error')} required">
	<label for="customer">
		<g:message code="phoneNumber.customer.label" default="Customer" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="customer" name="customer.id" from="${com.twc.Customer.list()}" optionKey="id" required="" value="${phoneNumberInstance?.customer?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: phoneNumberInstance, field: 'isActive', 'error')} ">
	<label for="isActive">
		<g:message code="phoneNumber.isActive.label" default="Is Active" />
		
	</label>
	<g:checkBox name="isActive" value="${phoneNumberInstance?.isActive}" />
</div>

<div class="fieldcontain ${hasErrors(bean: phoneNumberInstance, field: 'number', 'error')} ">
	<label for="number">
		<g:message code="phoneNumber.number.label" default="Number" />
		
	</label>
	<g:textField name="number" value="${phoneNumberInstance?.number}"/>
</div>

