<%@ page import="com.twc.Customer" %>



<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="customer.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${customerInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'phoneNumbers', 'error')} ">
	<label for="phoneNumbers">
		<g:message code="customer.phoneNumbers.label" default="Phone Numbers" />
		
	</label>
	<g:select name="phoneNumbers" from="${com.twc.PhoneNumber.list()}" multiple="multiple" optionKey="id" size="5" value="${customerInstance?.phoneNumbers*.id}" class="many-to-many"/>
</div>

