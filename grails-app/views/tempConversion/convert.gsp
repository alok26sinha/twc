<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tempConversion.label', default: 'TempConversion')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		<g:javascript library="jquery"/>
	</head>
	<body>
		<a href="#create-tempConversion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<%--<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		--%><div id="create-tempConversion" class="content scaffold-create" role="main">
			<h1><g:message code="default.conversion.label" args="[entityName]" default="Temperature Conversion Program"/></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${tempConversionInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${tempConversionInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<div id="convForm">
				<fieldset class="form">
				From Temperature	<g:field name="fromTemp" type="text"/></br></br>
				From Unit	<g:select name="fromUnit" from="${['degreeCelsius', 'degreeFahrenheit', 'degreeRankine', 'degreeReaumur', 'kelvin']}" value=""
          		noSelection="['':'-Choose from unit-']"/></br></br>
				To Unit 	<g:select name="toUnit" from="${['degreeCelsius', 'degreeFahrenheit', 'degreeRankine', 'degreeReaumur', 'kelvin']}" value=""
          		noSelection="['':'-Choose to unit-']"/></br></br>
				</fieldset>
				<fieldset class="buttons">
					<button onclick="callme()">Convert</button>
    
				</fieldset>
			</div>
		</div>
		</br>
		</br>
		<div id="converted"></div>
		</br>
		<script type="text/javascript">
		
		function callme(){
			//var formData = $('#convForm').serialize();
			var fromTemp =  $('#fromTemp').val();
			var fromUnit =  $('#fromUnit').val();
			var toUnit =  $('#toUnit').val();
			jQuery.ajax({type:'POST',
				data:  {
					fromTemp: fromTemp,
					fromUnit: fromUnit,
					toUnit: toUnit
					
				},
				url:'/TWC/tempConversion/callWs',
				success:function(data,textStatus){
					jQuery('#converted').html(data);
					},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
				});
			
			
			
			}
		</script>
	</body>
</html>
