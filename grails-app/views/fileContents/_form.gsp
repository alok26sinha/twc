<%@ page import="com.twc.FileContents" %>



<div class="fieldcontain ${hasErrors(bean: fileContentsInstance, field: 'userId', 'error')} required">
	<label for="userId">
		<g:message code="fileContents.userId.label" default="User Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userId" required="" value="${fileContentsInstance?.userId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileContentsInstance, field: 'amountOfCoins', 'error')} required">
	<label for="amountOfCoins">
		<g:message code="fileContents.amountOfCoins.label" default="Amount Of Coins" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="amountOfCoins" type="number" value="${fileContentsInstance.amountOfCoins}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: fileContentsInstance, field: 'userName', 'error')} required">
	<label for="userName">
		<g:message code="fileContents.userName.label" default="User Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userName" required="" value="${fileContentsInstance?.userName}"/>
</div>

