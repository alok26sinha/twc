
<%@ page import="com.twc.FileContents" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileContents.label', default: 'FileContents')}" />
		<title><g:message code="default.upload.label" args="[entityName]" /></title>
	</head>
	<body>
	<h1><g:message code="default.upload.label" args="[entityName]" default="Please upload the file containg data"/></h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		</br>
		<g:uploadForm action="upload">
		    <input type="file" name="file">
		    <g:submitButton name="upload" value="Upload"/>
		</g:uploadForm>
	</body>
</html>
