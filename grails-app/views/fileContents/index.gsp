
<%@ page import="com.twc.FileContents" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fileContents.label', default: 'FileContents')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-fileContents" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-fileContents" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="userId" title="${message(code: 'fileContents.userId.label', default: 'User Id')}" />
					
						<g:sortableColumn property="amountOfCoins" title="${message(code: 'fileContents.amountOfCoins.label', default: 'Amount Of Coins')}" />
					
						<g:sortableColumn property="userName" title="${message(code: 'fileContents.userName.label', default: 'User Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${fileContentsInstanceList}" status="i" var="fileContentsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${fileContentsInstance.id}">${fieldValue(bean: fileContentsInstance, field: "userId")}</g:link></td>
					
						<td>${fieldValue(bean: fileContentsInstance, field: "amountOfCoins")}</td>
					
						<td>${fieldValue(bean: fileContentsInstance, field: "userName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${fileContentsInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
