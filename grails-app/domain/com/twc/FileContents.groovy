package com.twc
import javax.management.modelmbean.RequiredModelMBean;

/**
 * The Class FileContents.
 */
class FileContents {

	/** The user id. */
	String userId

	/** The amount of coins. */
	Integer amountOfCoins

	/** The user name. */
	String userName

	/** The constraints. */
	static constraints = {
		userId (blank: false, size: 10..10)
		amountOfCoins (blank: false)
		userName (blank: false)
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FileContents [userId=" + userId + ", amountOfCoins="
		+ amountOfCoins + ", userName=" + userName + "]";
	}
}
