package com.twc


/**
 * The Class PhoneNumber.
 */
class PhoneNumber {
	
	/** The number. */
	String number
	
	/** The is active. */
	boolean isActive
	
	/** The belongs to. */
	static belongsTo = [customer: Customer]
    
    /** The constraints. */
    static constraints = {
    }
}
