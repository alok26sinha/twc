package com.twc


/**
 * The Class Customer.
 */
class Customer {
	
	/** The name. */
	String name
	
	/** The has many. */
	static hasMany = [phoneNumbers: PhoneNumber]
    
    /** The constraints. */
    static constraints = {
    }
}
