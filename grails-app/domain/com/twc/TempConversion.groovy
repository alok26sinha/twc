package com.twc


/**
 * The Class TempConversion.
 */
class TempConversion {
	
	/** The from temp. */
	Float fromTemp
	
	/** The to temp. */
	Float toTemp
	
	/** The from unit. */
	String fromUnit
	
	/** The to unit. */
	String toUnit
	
	//static transients = ['fromTemp', 'toTemp', 'fromUnit', 'toUnit']
    
    /** The constraints. */
    static constraints = {
    }
}
