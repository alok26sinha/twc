package com.twc



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


/**
 * The Class FileContentsController.
 */
@Transactional(readOnly = true)
class FileContentsController {

	/** The allowed methods. */
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	/**
	 * Index.
	 *
	 * @return the java.lang. object
	 */
	def index() {
		render(view: "upload")
	}
	
	/**
	 * Upload.
	 *
	 * @return the java.lang. object
	 */
	@Transactional
	def upload() {

		def count = 0
		params?.file?.inputStream?.toCsvReader()?.eachLine { tokens ->

			try{
				def fileContents = new FileContents()
				fileContents.userId = tokens[0]
				fileContents.amountOfCoins = Integer.parseInt(tokens[1]?.trim())
				fileContents.userName = tokens[2]
				
				if(!fileContents.validate()){
					
					throw new Exception(fileContents.errors.toString())
				}else{
					fileContents.save(flush:true)
				}
				count++
			}
			catch(Exception e){
				count++
				flash.message ="Error on line number "+count +", problem is "+e.getMessage()
				render(view: "upload")
				return;
			}
			flash.message ="File's content saved successfully"
			render(view: "upload")
		}
	}
	
	/**
	 * Show.
	 *
	 * @param fileContentsInstance the file contents instance
	 * @return the java.lang. object
	 */
	def show(FileContents fileContentsInstance) {
		respond fileContentsInstance
	}

	/**
	 * Creates the.
	 *
	 * @return the java.lang. object
	 */
	def create() {
		respond new FileContents(params)
	}

	/**
	 * Save.
	 *
	 * @param fileContentsInstance the file contents instance
	 * @return the java.lang. object
	 */
	@Transactional
	def save(FileContents fileContentsInstance) {
		if (fileContentsInstance == null) {
			notFound()
			return
		}

		if (fileContentsInstance.hasErrors()) {
			respond fileContentsInstance.errors, view:'create'
			return
		}

		fileContentsInstance.save flush:true

		request.withFormat {
			form {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'fileContentsInstance.label', default: 'FileContents'),
					fileContentsInstance.id
				])
				redirect fileContentsInstance
			}
			'*' { respond fileContentsInstance, [status: CREATED] }
		}
	}

	/**
	 * Edits the.
	 *
	 * @param fileContentsInstance the file contents instance
	 * @return the java.lang. object
	 */
	def edit(FileContents fileContentsInstance) {
		respond fileContentsInstance
	}

	/**
	 * Update.
	 *
	 * @param fileContentsInstance the file contents instance
	 * @return the java.lang. object
	 */
	@Transactional
	def update(FileContents fileContentsInstance) {
		if (fileContentsInstance == null) {
			notFound()
			return
		}

		if (fileContentsInstance.hasErrors()) {
			respond fileContentsInstance.errors, view:'edit'
			return
		}

		fileContentsInstance.save flush:true

		request.withFormat {
			form {
				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'FileContents.label', default: 'FileContents'),
					fileContentsInstance.id
				])
				redirect fileContentsInstance
			}
			'*'{ respond fileContentsInstance, [status: OK] }
		}
	}

	/**
	 * Delete.
	 *
	 * @param fileContentsInstance the file contents instance
	 * @return the java.lang. object
	 */
	@Transactional
	def delete(FileContents fileContentsInstance) {

		if (fileContentsInstance == null) {
			notFound()
			return
		}

		fileContentsInstance.delete flush:true

		request.withFormat {
			form {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'FileContents.label', default: 'FileContents'),
					fileContentsInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	/**
	 * Not found.
	 */
	protected void notFound() {
		request.withFormat {
			form {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'fileContentsInstance.label', default: 'FileContents'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
