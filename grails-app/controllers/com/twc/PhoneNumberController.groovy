package com.twc



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


/**
 * The Class PhoneNumberController.
 */
@Transactional(readOnly = true)
class PhoneNumberController {

    /** The allowed methods. */
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    /**
     * Index.
     *
     * @param max the max
     * @return the java.lang. object
     */
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PhoneNumber.list(params), model:[phoneNumberInstanceCount: PhoneNumber.count()]
    }

    /**
     * Show.
     *
     * @param phoneNumberInstance the phone number instance
     * @return the java.lang. object
     */
    def show(PhoneNumber phoneNumberInstance) {
        respond phoneNumberInstance
    }

    /**
     * Creates the.
     *
     * @return the java.lang. object
     */
    def create() {
        respond new PhoneNumber(params)
    }

    /**
     * Save.
     *
     * @param phoneNumberInstance the phone number instance
     * @return the java.lang. object
     */
    @Transactional
    def save(PhoneNumber phoneNumberInstance) {
        if (phoneNumberInstance == null) {
            notFound()
            return
        }

        if (phoneNumberInstance.hasErrors()) {
            respond phoneNumberInstance.errors, view:'create'
            return
        }

        phoneNumberInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'phoneNumberInstance.label', default: 'PhoneNumber'), phoneNumberInstance.id])
                redirect phoneNumberInstance
            }
            '*' { respond phoneNumberInstance, [status: CREATED] }
        }
    }

    /**
     * Edits the.
     *
     * @param phoneNumberInstance the phone number instance
     * @return the java.lang. object
     */
    def edit(PhoneNumber phoneNumberInstance) {
        respond phoneNumberInstance
    }

    /**
     * Update.
     *
     * @param phoneNumberInstance the phone number instance
     * @return the java.lang. object
     */
    @Transactional
    def update(PhoneNumber phoneNumberInstance) {
        if (phoneNumberInstance == null) {
            notFound()
            return
        }

        if (phoneNumberInstance.hasErrors()) {
            respond phoneNumberInstance.errors, view:'edit'
            return
        }

        phoneNumberInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PhoneNumber.label', default: 'PhoneNumber'), phoneNumberInstance.id])
                redirect phoneNumberInstance
            }
            '*'{ respond phoneNumberInstance, [status: OK] }
        }
    }

    /**
     * Delete.
     *
     * @param phoneNumberInstance the phone number instance
     * @return the java.lang. object
     */
    @Transactional
    def delete(PhoneNumber phoneNumberInstance) {

        if (phoneNumberInstance == null) {
            notFound()
            return
        }

        phoneNumberInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PhoneNumber.label', default: 'PhoneNumber'), phoneNumberInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    /**
     * Not found.
     */
    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'phoneNumberInstance.label', default: 'PhoneNumber'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
