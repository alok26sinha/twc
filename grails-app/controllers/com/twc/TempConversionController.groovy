package com.twc



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.grails.plugins.wsclient.service.WebService


/**
 * The Class TempConversionController.
 */
@Transactional(readOnly = true)
class TempConversionController {
	
	/** The web service. */
	WebService webService
    
    /** The allowed methods. */
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    /**
     * Index.
     *
     * @return the java.lang. object
     */
    def index() {
      
    }

    

    /**
     * Convert.
     *
     * @return the java.lang. object
     */
    def convert() {
        respond new TempConversion(params)
    }
	
	/**
	 * Call ws.
	 *
	 * @return the java.lang. object
	 */
	def callWs() {
		
		def wsdlURL = "http://www.webservicex.net/ConvertTemperature.asmx?WSDL"
		def proxy = webService.getClient(wsdlURL)

		def result = proxy.ConvertTemp(params.fromTemp, params.fromUnit, params.toUnit)
		
		render(status: 200, text: '&nbsp;&nbsp;&nbsp;&nbsp; Converted Temperature is '+result +' '+params.toUnit)
	}
   
}
