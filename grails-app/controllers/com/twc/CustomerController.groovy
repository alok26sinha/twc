
package com.twc



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import com.twc.PhoneNumberService


/**
 * The Class CustomerController.
 */
@Transactional(readOnly = true)
class CustomerController {
	
	/** The phone number service. */
	def phoneNumberService 
    
    /** The allowed methods. */
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    /**
     * Index.
     *
     * @param max the max
     * @return the java.lang. object
     */
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Customer.list(params), model:[customerInstanceCount: Customer.count()]
    }

    /**
     * Show.
     *
     * @param customerInstance the customer instance
     * @return the java.lang. object
     */
    def show(Customer customerInstance) {
        respond customerInstance
    }

    /**
     * Creates the.
     *
     * @return the java.lang. object
     */
    def create() {
        respond new Customer(params)
    }

    /**
     * Save.
     *
     * @param customerInstance the customer instance
     * @return the java.lang. object
     */
    @Transactional
    def save(Customer customerInstance) {
        if (customerInstance == null) {
            notFound()
            return
        }

        if (customerInstance.hasErrors()) {
            respond customerInstance.errors, view:'create'
            return
        }

        customerInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'customerInstance.label', default: 'Customer'), customerInstance.id])
                redirect customerInstance
            }
            '*' { respond customerInstance, [status: CREATED] }
        }
    }

    /**
     * Edits the.
     *
     * @param customerInstance the customer instance
     * @return the java.lang. object
     */
    def edit(Customer customerInstance) {
        respond customerInstance
    }

    /**
     * Update.
     *
     * @param customerInstance the customer instance
     * @return the java.lang. object
     */
    @Transactional
    def update(Customer customerInstance) {
        if (customerInstance == null) {
            notFound()
            return
        }

        if (customerInstance.hasErrors()) {
            respond customerInstance.errors, view:'edit'
            return
        }

        customerInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Customer.label', default: 'Customer'), customerInstance.id])
                redirect customerInstance
            }
            '*'{ respond customerInstance, [status: OK] }
        }
    }

    /**
     * Delete.
     *
     * @param customerInstance the customer instance
     * @return the java.lang. object
     */
    @Transactional
    def delete(Customer customerInstance) {

        if (customerInstance == null) {
            notFound()
            return
        }

        customerInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Customer.label', default: 'Customer'), customerInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    /**
     * Not found.
     */
    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerInstance.label', default: 'Customer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	/**
	 * Test.
	 *
	 * @return the java.lang. object
	 */
	def test(){
		println(// TODO: Auto-generated Javadoc

/**
 * Run.
 *
 * @return the java.lang. object
 */
phoneNumberService.getAllPhoneNumbers())
	}
}
