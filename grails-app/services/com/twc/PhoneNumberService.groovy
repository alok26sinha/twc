package com.twc

import grails.transaction.Transactional

@Transactional
class PhoneNumberService {

    def serviceMethod() {

    }
	
	def getAllPhoneNumbers(){
		return PhoneNumber.getAll()
	}
	
	def getAllPhoneNumbersForCustomer(String name){
		def customer = Customer.findByName(name)
		return customer.phoneNumbers
	}
	
	def activatePhoneNumber(String number){
		def phoneNumber = PhoneNumber.findByNumber(number)
		phoneNumber.isActive = true
		return phoneNumber.save(flush:true)
	}
}
