package com.twc



import grails.test.mixin.*
import spock.lang.*

@TestFor(FileContentsController)
@Mock(FileContents)
class FileContentsControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.fileContentsInstanceList
            model.fileContentsInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.fileContentsInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def fileContents = new FileContents()
            fileContents.validate()
            controller.save(fileContents)

        then:"The create view is rendered again with the correct model"
            model.fileContentsInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            fileContents = new FileContents(params)

            controller.save(fileContents)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/fileContents/show/1'
            controller.flash.message != null
            FileContents.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def fileContents = new FileContents(params)
            controller.show(fileContents)

        then:"A model is populated containing the domain instance"
            model.fileContentsInstance == fileContents
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def fileContents = new FileContents(params)
            controller.edit(fileContents)

        then:"A model is populated containing the domain instance"
            model.fileContentsInstance == fileContents
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/fileContents/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def fileContents = new FileContents()
            fileContents.validate()
            controller.update(fileContents)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.fileContentsInstance == fileContents

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            fileContents = new FileContents(params).save(flush: true)
            controller.update(fileContents)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/fileContents/show/$fileContents.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/fileContents/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def fileContents = new FileContents(params).save(flush: true)

        then:"It exists"
            FileContents.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(fileContents)

        then:"The instance is deleted"
            FileContents.count() == 0
            response.redirectedUrl == '/fileContents/index'
            flash.message != null
    }
}
